/*
 Copyright (C) by Carlos Saavedra <clsaavedrag@gmail.com> 2016

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, see <http://www.gnu.org/licenses/>.
 */
/* A FUSE filesystem based on libcmis-c. */

#define FUSE_USE_VERSION 26
#define _FILE_OFFSET_BITS 64

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <getopt.h>
#include <syslog.h>
#include <libgen.h>
#include <magic.h>
#include <time.h>

#include <fuse.h>
#include <libcmis-c/libcmis-c.h>

#define CMIS_CONTENT_STREAM_LENGTH "cmis:contentStreamLength"
#define CMIS_FOLDER_TYPE "cmis:folder"
#define CMIS_DOCUMENT_TYPE "cmis:document"
#define CMIS_NAME_PROPERTY "cmis:name"
#define CMIS_OBJECTTYPEID_PROPERTY "cmis:objectTypeId"

// static const int ECMISPARAM = 10;
static const int ECMISPARAMURL = 11;
static const int ECMISPARAMMNT = 12;
static const int ECMISPARAMUSR = 13;
static const int ECMISPARAMPWD = 14;
static const int ECMISPARAMREP = 15;
static const int ECMISSESSIONCREATE = 20;

static libcmis_SessionPtr session = NULL;
static size_t length = 0;
static void *content = NULL;

static void fuse_cmis_error(const char *function, libcmis_ErrorPtr error) {
	const char *mssg = libcmis_error_getMessage(error);
	const char *type = libcmis_error_getType(error);
	syslog(LOG_ERR, "[%s] message %s type %s", function, mssg, type);
}

static const char *fuse_cmis_contentType(const void *buf, size_t size) {
	magic_t cookie = magic_open(MAGIC_MIME);
	if(cookie == NULL) {
		syslog(LOG_ERR, "fuse_cmis_contentType: unable to initialize magic library");
	}
	if(magic_load(cookie, NULL) != 0) {
		syslog(LOG_ERR, "fuse_cmis_contentType: can not load magic database %s", magic_error(cookie));
		magic_close(cookie);
	}
	const char *contentType = strdup("application/octet-stream");
	if(buf)
		contentType = magic_buffer(cookie, buf, size);
	if(!contentType) {
		int err = magic_errno(cookie);
		const char *str = magic_error(cookie);
		syslog(LOG_ERR, "fuse_cmis_contentType - error: %d - %s", err, (str != NULL) ? str : "-- no-description --");
		contentType = strdup("application/octet-stream");
	}
	magic_close(cookie);
	return contentType;
}

static int fuse_cmis_getattr(const char *path, struct stat *stbuf) {
	int ret = 0;
	libcmis_ErrorPtr error = libcmis_error_create();
	libcmis_ObjectPtr cmisObj = libcmis_session_getObjectByPath(session, path,
			error);
	if (cmisObj != NULL) {
		bool isFolder = libcmis_is_folder(cmisObj);
		bool isDocument = libcmis_is_document(cmisObj);
		if (isFolder) { // Is a folder or file?
			stbuf->st_mode = S_IFDIR | 0770; // TODO Set the real mode
			stbuf->st_nlink = 2;
		} else if (isDocument) {
			stbuf->st_mode = S_IFREG | 0660; // TODO Set the real mode
			stbuf->st_nlink = 1;
		}
		libcmis_PropertyPtr property = libcmis_object_getProperty(cmisObj,
		CMIS_CONTENT_STREAM_LENGTH);
		libcmis_vector_long_Ptr cmisObjectSize = libcmis_property_getLongs(
				property);
		stbuf->st_size = libcmis_vector_long_get(cmisObjectSize, 0);

		stbuf->st_uid = (int) getuid();
		stbuf->st_gid = (int) getgid();
		stbuf->st_mtim.tv_sec =
				libcmis_object_getLastModificationDate(cmisObj);
		stbuf->st_ctim.tv_nsec =
				libcmis_object_getCreationDate(cmisObj);

		libcmis_object_free(cmisObj);
	} else {
		fuse_cmis_error("fuse_cmis_getattr", error);
		ret = -ENOENT; // File or folder doesn't exist
	}
	libcmis_error_free(error);
	return ret;
}

/*
 * cmis command: show-by-path
 */
static int fuse_cmis_readdir(const char *path, void *buf,
		fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi) {
	int ret = 0;
	libcmis_ErrorPtr error = libcmis_error_create();
	libcmis_ObjectPtr cmisObj = libcmis_session_getObjectByPath(session, path,
			error);
	if (cmisObj != NULL) {
		bool isFolder = libcmis_is_folder(cmisObj);
		if (isFolder) {
			struct stat stbuf;
			libcmis_FolderPtr parent = libcmis_folder_cast(cmisObj);

			libcmis_vector_object_Ptr children = libcmis_folder_getChildren(
					parent, error);
			if (children != NULL) {
				size_t pos, elements = libcmis_vector_object_size(children);
				int entryoffset, entrylen, nextoffset = 0;
				for (pos = 0; pos < elements; pos++) {
					libcmis_ObjectPtr child = libcmis_vector_object_get(
							children, pos);
					char *childname = libcmis_object_getName(child);
					entrylen = ((24 + strlen(childname) + 7) & ~7);
					entryoffset = nextoffset;
					nextoffset += entrylen;
					libcmis_object_free(child);
					if (entryoffset < offset)
						continue;
					if (filler(buf, childname, &stbuf, nextoffset))
						break;
				}
				libcmis_vector_object_free(children);
			}
			filler(buf, ".", &stbuf, 0);
			filler(buf, "..", &stbuf, 0);
		} else {
			ret = -ENOTDIR;
		}
		libcmis_object_free(cmisObj);
	} else {
		fuse_cmis_error("fuse_cmis_readdir", error);
		ret = -ENOENT; // File or folder doesn't found
	}
	libcmis_error_free(error);
	return ret;
}

/*
static int fuse_cmis_readlink(const char *path, char *buf, size_t size) {
	return 0;
}
*/

static int fuse_cmis_open(const char *path, struct fuse_file_info *fi) {
	int ret = 0;
	syslog(LOG_DEBUG, "fuse_cmis_open %s", path);
	libcmis_ErrorPtr error = libcmis_error_create();
	libcmis_ObjectPtr cmisObj = NULL;
	if(!fi->fh) {
		cmisObj = libcmis_session_getObjectByPath(session, path,
				error);
		if (cmisObj != NULL) {
			bool isDocument = libcmis_is_document(cmisObj);
			if (isDocument) {
				if(fi == NULL) {
					fi = (struct fuse_file_info *) malloc(
							sizeof(struct fuse_file_info));
				}
				fi->fh = (uint64_t) cmisObj;
				libcmis_DocumentPtr document = libcmis_document_cast(cmisObj);
				length = libcmis_document_getContentLength(document);
				if(content)
					free(content);
				content = (void *)malloc(sizeof(char) * length);
				FILE *fmem = fmemopen(content, length, "w+");
				libcmis_document_getContentStream(document,
						(libcmis_writeFn) fwrite, fmem, error);
				fflush(fmem);
				fclose(fmem);
			}
		} else {
			fuse_cmis_error("fuse_cmis_open", error);
			ret = -ENOENT; // File or folder doesn't found
		}
	}
	libcmis_error_free(error);
	return ret;
}

static int fuse_cmis_release(const char *path, struct fuse_file_info *fi) {
	int ret = 0;
	syslog(LOG_DEBUG, "fuse_cmis_release %s", path);
	if(fi && fi->fh) {
		libcmis_ObjectPtr cmisObj = (libcmis_ObjectPtr)fi->fh;
		bool isDocument = libcmis_is_document(cmisObj);
		if(isDocument) {
			if(length > 0 && content != NULL) {
				libcmis_ErrorPtr error = libcmis_error_create();
				libcmis_DocumentPtr document = libcmis_document_cast(cmisObj);
				const char *contentType = fuse_cmis_contentType(content, length);

				// Write the new content to cmis repository
				FILE *fmem = fmemopen(content, length, "r");
				char *filename = basename(strdup(path));

				libcmis_document_setContentStream(document, (libcmis_readFn)fread,
						fmem, contentType, filename, true, error);
				syslog(LOG_DEBUG, "fuse_cmis_release %s", path);
				fflush(fmem);
				fclose(fmem);
				free(content);
				length = 0;
				if(libcmis_error_getType(error) != NULL) {
					fuse_cmis_error("fuse_cmis_release:1", error);
					ret = -EIO;
				}
			}
			libcmis_document_free(libcmis_document_cast(cmisObj));
		} else {
			libcmis_folder_free(libcmis_folder_cast(cmisObj));
		}
	}
	return ret;
}

static int fuse_cmis_read(const char *path, char *buf, size_t size,
		off_t offset, struct fuse_file_info *fi) {
	int ret = 0;
	libcmis_ErrorPtr error = libcmis_error_create();
	libcmis_ObjectPtr cmisObj = (fi->fh) ?
	  (libcmis_ObjectPtr)fi->fh :
	  libcmis_session_getObjectByPath(session, path, error);
	if (cmisObj != NULL) {
		bool isDocument = libcmis_is_document(cmisObj);
		if (isDocument) {
			libcmis_DocumentPtr document = libcmis_document_cast(cmisObj);
			long len = libcmis_document_getContentLength(document);
			if(offset < len) {
				FILE *fmem = fmemopen(buf, size, "w+");
				libcmis_document_getContentStream(document,
					(libcmis_writeFn) fwrite, fmem, error);
				fflush(fmem);
				fclose(fmem);
				ret = (offset + size > len) ?
				  len - offset : size;
			}
		} else {
			ret = -EINVAL;
		}
	} else {
		fuse_cmis_error("fuse_cmis_read", error);
		ret = -ENOENT; // File or folder doesn't found
	}
	libcmis_error_free(error);
	return ret;
}

static libcmis_FolderPtr fuse_cmis_getParentFolder(char *path, libcmis_ErrorPtr error) {
	char *parentPath = dirname(path);
	libcmis_FolderPtr parentFolder = NULL;
	if(!strcmp(parentPath, "/")) {
		parentFolder = libcmis_session_getRootFolder(session, error);
	} else {
		libcmis_ObjectPtr parent = libcmis_session_getObjectByPath(session, parentPath, error);
		if(parent != NULL && libcmis_is_folder(parent)) {
			parentFolder = libcmis_folder_cast(parent);
		}
	}
	return parentFolder;
}

static libcmis_vector_property_Ptr fuse_cmis_properties(const char *name, const char *object_cmis_type, libcmis_ErrorPtr error) {
	libcmis_vector_property_Ptr properties = NULL;
	libcmis_ObjectTypePtr type = libcmis_session_getType(session, CMIS_DOCUMENT_TYPE, error);
	if(type != NULL) {
		libcmis_vector_property_type_Ptr propertiesTypes = libcmis_object_type_getPropertiesTypes(type);
		size_t nProperties = libcmis_vector_property_type_size(propertiesTypes);
		properties = libcmis_vector_property_create();
		for(size_t i = 0; i < nProperties; i++) {
			libcmis_PropertyTypePtr propertyType = libcmis_vector_property_type_get(propertiesTypes, i);
			char *id = libcmis_property_type_getId(propertyType);
			const char *value[1] = { NULL };
			if(!strcmp(id, CMIS_NAME_PROPERTY)) {
				value[0] = name;
			} else if(!strcmp(id, CMIS_OBJECTTYPEID_PROPERTY)) {
				value[0] = object_cmis_type;
			}
			if(value[0] != NULL) {
				libcmis_PropertyPtr property = libcmis_property_create(propertyType, value, 1);
				if(property)
					libcmis_vector_property_append(properties, property);
			}
		}
	} else {
		fuse_cmis_error("fuse_cmis_document_properties", error);
	}
	return properties;
}

static int fuse_cmis_write(const char *path, const char *buf, size_t size,
		off_t offset, struct fuse_file_info *fi) {
	int ret = 0;
	libcmis_ErrorPtr error = libcmis_error_create();
	libcmis_ObjectPtr object = (libcmis_ObjectPtr)fi->fh;

	if(!object)
		object = libcmis_session_getObjectByPath(session, path, error);
	if(object && libcmis_is_document(object)) {
		size_t contentSize = length;
		if(offset + size > length)
			contentSize = offset + size;
		syslog(LOG_DEBUG, "fuse_cmis_write: size: %lu offset: %lu, length: %lu contentSize: %lu", size, offset, length, contentSize);
		void *data = (void *)malloc(sizeof(char) * contentSize);
		if(length > 0 && content != NULL) {
			memcpy(data, content, length);
		}

		// Add new data to the content
		memcpy(data + offset, buf, size);
		if(length > 0) {
			free(content);
		}
		content = data;
		length = contentSize;
		ret = size;
	} else {
		fuse_cmis_error("fuse_cmis_write:2", error);
		ret = -ENOENT; // File not found error
	}
	return ret;
}

static int fuse_cmis_create(const char *path, mode_t mode,
		struct fuse_file_info *fi) {
	int ret = 0;
	libcmis_ErrorPtr error = libcmis_error_create();
	libcmis_ObjectPtr cmisObj = libcmis_session_getObjectByPath(session, path,
			error);
	if (cmisObj) {
		libcmis_object_free(cmisObj);
		ret = -EEXIST;
	} else {
		libcmis_FolderPtr parentFolder = fuse_cmis_getParentFolder(strdup(path), error);
		if (parentFolder) {
			char *name = basename(strdup(path));
			libcmis_vector_property_Ptr documentProperties =
					fuse_cmis_properties(name, CMIS_DOCUMENT_TYPE, error);
			if (documentProperties) {
				FILE *fmem = fmemopen(" ", (size_t) 1, "r");
				libcmis_DocumentPtr doc = libcmis_folder_createDocument(
						parentFolder, documentProperties, (libcmis_readFn) fread,
						fmem, "application/octet-stream", name, error);
				fclose(fmem);
				if (doc == NULL) {
					fuse_cmis_error("fuse_cmis_create", error);
					ret = -EACCES;
				} else {
					syslog(LOG_DEBUG, "fuse_cmis_create %s", path);
					fi->fh = (uint64_t)doc;
				}
				libcmis_vector_property_free(documentProperties);
			}
		} else {
			ret = -EINVAL;
		}
	}
	libcmis_error_free(error);
	return ret;
}

static int fuse_cmis_utime(const char *path, struct utimbuf *times) {
	return 0;
}

static int fuse_cmis_unlink(const char *path) {
	int ret = 0;
	libcmis_ErrorPtr error = libcmis_error_create();
	libcmis_ObjectPtr cmisObj = libcmis_session_getObjectByPath(session, path,
			error);
	if(cmisObj) {
		bool isDocument = libcmis_is_document(cmisObj);
		bool isFolder = libcmis_is_folder(cmisObj);
		if(isDocument) {
			libcmis_AllowableActionsPtr allowable =
					libcmis_object_getAllowableActions(cmisObj);
			bool isAllowRemove = libcmis_allowable_actions_isAllowed(
					allowable, libcmis_DeleteObject);
			libcmis_allowable_actions_free(allowable);
			if(isAllowRemove)
				libcmis_object_remove(cmisObj, true, error);
			else
				ret = -EPERM;
		} else if(isFolder) {
			ret = -EISDIR;
		} else {
			ret = -EINVAL;
		}
	} else {
		ret = -ENOENT;
	}
	return ret;
}

static int fuse_cmis_rmdir(const char *path) {
	int ret = 0;
	libcmis_ErrorPtr error = libcmis_error_create();
	libcmis_ObjectPtr cmisObj = libcmis_session_getObjectByPath(session, path,
			error);
	if(cmisObj) {
		bool isDocument = libcmis_is_document(cmisObj);
		bool isFolder = libcmis_is_folder(cmisObj);
		if(isFolder) {
			libcmis_FolderPtr folder = libcmis_folder_cast(cmisObj);
			libcmis_vector_object_Ptr objects = libcmis_folder_getChildren(
					folder, error);
			if(!objects || libcmis_vector_object_size(objects) == 0) {
				libcmis_AllowableActionsPtr allowable =
						libcmis_object_getAllowableActions(cmisObj);
				bool isAllowRemove = libcmis_allowable_actions_isAllowed(
						allowable, libcmis_DeleteObject);
				libcmis_allowable_actions_free(allowable);
				if(isAllowRemove)
					libcmis_object_remove(cmisObj, true, error);
				else
					ret = -EPERM;
			} else {
				ret = -ENOTEMPTY;
			}
		} else if(isDocument) {
			ret = -ENOTDIR;
		} else {
			ret = -EINVAL;
		}
	} else {
		ret = -ENOENT;
	}
	return ret;
}

static int fuse_cmis_mkdir(const char *path, mode_t mode) {
	int ret = 0;
	char *name = basename(strdup(path)); // Get the folder name
	char *parentPath = dirname(strdup(path)); // Get the parent path
	libcmis_ErrorPtr error = libcmis_error_create();
	libcmis_FolderPtr parentFolder = NULL;
	if(!strcmp(parentPath, "/")) {
		parentFolder = libcmis_session_getRootFolder(session, error);
	} else {
		libcmis_ObjectPtr parent = libcmis_session_getObjectByPath(session, parentPath, error);
		if(parent != NULL && libcmis_is_folder(parent)) {
			parentFolder = libcmis_folder_cast(parent);
		}
	}
	if(parentFolder != NULL) {
		libcmis_vector_property_Ptr folderProperties = fuse_cmis_properties(name, CMIS_FOLDER_TYPE, error);
		if(folderProperties != NULL) {
			libcmis_FolderPtr folder = libcmis_folder_createFolder(parentFolder, folderProperties, error);
			if(folder == NULL) {
				fuse_cmis_error("fuse_cmis_mkdir:1", error);
				ret = -EACCES;
			}
			libcmis_vector_property_free(folderProperties);
			libcmis_folder_free(folder);
		} else {
			fuse_cmis_error("fuse_cmis_mkdir:2", error);
			ret = -EPERM;
		}
	} else {
		fuse_cmis_error("fuse_cmis_mkdir:3", error);
		ret = -ENOENT;
	}
	libcmis_folder_free(parentFolder);
	libcmis_error_free(error);
	return ret;
}

/*
static int fuse_cmis_mknod(const char *path, mode_t mode, dev_t rdev) {
	return 0;
}
*/

/*
static int fuse_cmis_symlink(const char *from, const char *to) {
	return 0;
}
*/

static int fuse_cmis_rename(const char *from, const char *to) {
	libcmis_ErrorPtr error = libcmis_error_create();

	// Get the source object (folder or file) and its parent folder.
	libcmis_ObjectPtr object = libcmis_session_getObjectByPath(session, from, error);
	if(!object) {
		// from object does not exist
		fuse_cmis_error("fuse_cmis_rename.1", error);
		return -ENOENT;
	}

	// Check if 'from' and 'to' folders are the same, if not ...
	if(strcmp(dirname(strdup(from)), dirname(strdup(to)))) {
		bool isOldObjectFolder = libcmis_is_folder(object);
		libcmis_FolderPtr fromFolder = NULL;
		if(!isOldObjectFolder) {
			libcmis_vector_folder_Ptr fromParentsFolders = libcmis_document_getParents(
					libcmis_document_cast(object), error);
			if(fromParentsFolders) {
				size_t numFolder = libcmis_vector_folder_size(fromParentsFolders);
				if(numFolder > 0) {
					size_t i = 0;
					while(!fromFolder && numFolder > i) {
						fromFolder = libcmis_vector_folder_get(fromParentsFolders, i++);
					}
				}
			} else {
				fuse_cmis_error("fuse_cmis_rename.2", error);
			}
			if(!fromFolder) {
				fromFolder = fuse_cmis_getParentFolder(strdup(from), error);
			}
		} else {
			fromFolder = libcmis_folder_getParent(libcmis_folder_cast(object), error);
			if(!fromFolder) {
				fuse_cmis_error("fuse_cmis_rename.3", error);
			}
		}

		// Get the destination object (folder or file).
		libcmis_ObjectPtr newObject = libcmis_session_getObjectByPath(session, to, error);
		bool isNewObjectFolder = libcmis_is_folder(newObject);
		libcmis_FolderPtr destFolder = NULL;
		// If destination object exist ...
		if(newObject) {
			// . it must be a folder.
			if(!isNewObjectFolder) {
				return -ENOTEMPTY;
			}
			destFolder = libcmis_folder_cast(newObject);
		} else {
			destFolder = fuse_cmis_getParentFolder(strdup(to), error);
			if(!destFolder) {
				fuse_cmis_error("fuse_cmis_rename.4", error);
			}
		}

		// Move from source folder to destination folder
		if(object && fromFolder && destFolder) {
			libcmis_object_move(object, fromFolder, destFolder, error);
		}
	}

	// Change the file name if its not same ...
	if(strcmp(basename(strdup(from)), basename(strdup(to)))) {
		// rename the from object to new name.
		const char *newFileName[1] = { NULL };
		newFileName[0] = basename(strdup(to));
		libcmis_vector_property_Ptr properties = libcmis_object_getProperties(object);
		if(properties) {
			libcmis_vector_property_Ptr newProperties = libcmis_vector_property_create();
			size_t sizeProperties = libcmis_vector_property_size(properties);
			bool find = false;
			for(size_t i = 0; i < sizeProperties && !find; i++) {
				libcmis_PropertyPtr property = libcmis_vector_property_get(properties, i);
				libcmis_PropertyTypePtr propertyType = libcmis_property_getPropertyType(property);
				char *propName = libcmis_property_type_getId(propertyType);
				if((find = !strcmp(propName, CMIS_NAME_PROPERTY))) {
					libcmis_PropertyPtr newNameProperty = libcmis_property_create(propertyType, newFileName, 1);
					libcmis_vector_property_append(newProperties, newNameProperty);
				}
			}
			if(find && !libcmis_object_updateProperties(object, newProperties, error)) {
				fuse_cmis_error("fuse_cmis_rename.5", error);
			}
		}
	}
	libcmis_error_free(error);

	return 0;
}

/*
static int fuse_cmis_link(const char *from, const char *to) {
	return 0;
}
*/

/*
static int fuse_cmis_chmod(const char *path, mode_t mode) {
	return 0;
}
*/

/*
static int fuse_cmis_chown(const char *path, uid_t uid, gid_t gid) {
	return 0;
}
*/

static int fuse_cmis_truncate(const char *path, off_t size) {
	return 0;
}

static int fuse_cmis_fsync(const char *path, int isdatasync,
		struct fuse_file_info *fi) {
	int ret = 0;
	return ret;
}


static struct fuse_operations cmis_oper = {
		/*.chmod = fuse_cmis_chmod,*/
		/*.chown = fuse_cmis_chown,*/
		.create = fuse_cmis_create,
		.fsync = fuse_cmis_fsync,
		.getattr = fuse_cmis_getattr,
		/*.link = fuse_cmis_link,*/
		.mkdir = fuse_cmis_mkdir,
		/*.mknod = fuse_cmis_mknod,*/
		.open = fuse_cmis_open,
		.read = fuse_cmis_read,
		.readdir = fuse_cmis_readdir,
		/*.readlink = fuse_cmis_readlink,*/
		.release = fuse_cmis_release,
		.rmdir = fuse_cmis_rmdir,
		.unlink = fuse_cmis_unlink,
		.utime = fuse_cmis_utime,
		.rename = fuse_cmis_rename,
		/*.symlink = fuse_cmis_symlink,*/
		.truncate = fuse_cmis_truncate,
		.write = fuse_cmis_write,
};

void print_usage(char *name) {
	printf(
			"Usage: %s [-?|--help] [-c|--cmis-share=cmis-url] [-u|--username=username] [-p|--password=password] [-r|--repository=repositoryId] [-m|--mountpoint=mountpoint] [-f|--foreground] [-d|--debug] [-S|--no-ssl-check]\n",
			name);
	exit(0);
}

int main(int argc, char *argv[]) {
	openlog("fuse-cmis", LOG_PID, LOG_USER); // Open syslog
	syslog(LOG_DEBUG, "pid=%d:ppid=%d", getpid(), getppid());

	int ret = 0;
	static struct option long_opts[] = {
			{ "help", no_argument, 0, '?' },
			{ "cmis-share", required_argument, 0, 'c' },
			{ "mountpoint", required_argument, 0, 'm' },
			{ "repository ", required_argument, 0, 'r' },
			{ "username", required_argument, 0, 'u' },
			{ "password", required_argument, 0, 'p' },
			{ "no-ssl-check", no_argument, 0, 'S' },
			{ "verbose", no_argument, 0, 'v' },
			{ "debug", no_argument, 0, 'd' },
			{ "foreground", no_argument, 0, 'f' },
			{ NULL, 0, 0, 0 }
	};

	int c;
	int opt_idx = 0;
	char *url = NULL;
	char *mnt = NULL;
	char *username = NULL;
	char *password = NULL;
	char *repositoryId = NULL;
	bool noSslCheck = false; // Must have a valid certificate.
	bool verbose = false;
	libcmis_OAuth2DataPtr oauth2 = NULL;
	int fuse_cmis_argc = 5;
	char *fuse_cmis_argv[16] = {
			"fuse-cmis",
			"<export>",
			"-odefault_permissions",
			"-omax_write=32769",
			"-s",
			NULL,
			NULL,
			NULL,
			NULL,
			NULL,
			NULL,
			NULL,
			NULL,
			NULL,
			NULL,
			NULL, };

	while ((c = getopt_long(argc, argv, "?ham:n:", long_opts, &opt_idx)) > 0) {
		switch (c) {
		case 'h':
		case '?':
			print_usage(argv[0]);
			return 0;
		case 'm':
			mnt = strdup(optarg);
			break;
		case 'c':
			url = strdup(optarg);
			break;
		case 'u':
			username = strdup(optarg);
			break;
		case 'p':
			password = strdup(optarg);
			break;
		case 'r':
			repositoryId = strdup(optarg);
			break;
		case 'S':
			noSslCheck = true; // Ignore invalid certificates
			break;
		case 'v':
			verbose = true;
			break;
		case 'f':
			fuse_cmis_argv[fuse_cmis_argc++] = strdup("-f");
			break;
		case 'd':
			fuse_cmis_argv[fuse_cmis_argc++] = strdup("-d");
			break;
		}
	}

	if (url == NULL) {
		fprintf(stderr, "-c|--cmis-share was not specified.\n");
		print_usage(argv[0]);
		ret = -ECMISPARAMURL;
		goto finished;
	}

	if (mnt == NULL) {
		fprintf(stderr, "-m|--mountpoint was not specified.\n");
		print_usage(argv[0]);
		ret = -ECMISPARAMMNT;
		goto finished;
	}

	if (username == NULL) {
		fprintf(stderr, "-u|--username was not specified.\n");
		print_usage(argv[0]);
		ret = -ECMISPARAMUSR;
		goto finished;
	}

	if (password == NULL) {
		fprintf(stderr, "-p|--password was not specified.\n");
		print_usage(argv[0]);
		ret = -ECMISPARAMPWD;
		goto finished;
	}

	if (repositoryId == NULL) {
		fprintf(stderr, "-r|--repository was not specified.\n");
		print_usage(argv[0]);
		ret = -ECMISPARAMREP;
		goto finished;
	}

	fuse_cmis_argv[1] = mnt;
	static char *fuse_cmis_userdata_argv[16] = {
			"<username>",
			"<password>",
			"<repositoryId>",
			"<no-ssl-check>",
			"<verbose>",
			NULL,
			NULL,
			NULL,
			NULL,
			NULL,
			NULL,
			NULL,
			NULL,
			NULL,
			NULL,
			NULL, };

	fuse_cmis_userdata_argv[0] = username;
	fuse_cmis_userdata_argv[1] = password;
	fuse_cmis_userdata_argv[2] = repositoryId;
	fuse_cmis_userdata_argv[3] = (noSslCheck) ? "t\0" : "f\0";
	fuse_cmis_userdata_argv[4] = (verbose) ? "t\0" : "f\0";

	libcmis_ErrorPtr error = libcmis_error_create();
	session = libcmis_createSession(url, repositoryId, username, password,
			noSslCheck, oauth2, // TODO Create a oauth2 object
			verbose, error);
	if (session == NULL) {
		fuse_cmis_error("main", error);
		ret = -ECMISSESSIONCREATE;
	} else {
		return fuse_main(fuse_cmis_argc, fuse_cmis_argv, &cmis_oper,
				fuse_cmis_userdata_argv);
	}
	finished: free(url);
	free(mnt);
	free(username);
	free(password);
	return ret;
}
